#!/usr/bin/env python3
"""
Collect temperature readings from MQTT broker.
"""

import asyncio
import logging
import re
import signal
import sqlite3
import sys
from argparse import ArgumentParser, Namespace
from datetime import datetime, timezone
import socket
from functools import lru_cache
from typing import Optional, List, Iterable, Sequence
from urllib.parse import urljoin
from uuid import uuid4

import aiohttp
import aiosqlite
import pydantic
from aiohttp import ClientConnectorError
from asyncio_mqtt import Client, Will, MqttError
from paho.mqtt.client import MQTTMessage  # type: ignore

from log_config import RootLogging, Logging, LogLevel
import grid


class Reading(pydantic.BaseModel):
    """
    Model for one reading.
    """

    device: str
    value: float
    date: datetime = pydantic.Field(default_factory=lambda: datetime.now(tz=timezone.utc))


class Readings(pydantic.BaseModel):
    """
    Model containing list of readings.
    """

    __root__: List[Reading]


class MqttConfig(pydantic.BaseModel):
    """
    MQTT configuration
    """

    host: str = "localhost"
    port: int = 1883
    user: Optional[str] = None
    password: Optional[str] = None
    client_id: Optional[str] = None
    lwt_topic: Optional[str] = None
    lwt_online: Optional[str] = None
    lwt_offline: Optional[str] = None
    lwt_retain: bool = True
    topic: str = "sensorio/{device}/channel/{sensor}/{unit}"

    @staticmethod
    @lru_cache
    def re_for_topic(topic: str) -> re.Pattern:
        """
        Return compiled RE for given topic.
        :param topic: Topic to compile into RE.
        :return: Compiled regexp partern.
        """
        logging.debug("Compiling topic...")
        return re.compile(
            re
            .escape(topic)
            .replace("\\+", "[^/]*")
            .replace("#", ".*?")
            .replace("\\}", "}")
            .replace("\\{", "{")
            .format(
                sensor="(?P<sensor>[^/]*)",
                device="(?P<device>[^/]*)",
                unit="(?P<unit>[^/]*)",
            )
        )

    @property
    def topic_re(self) -> re.Pattern:
        """
        Return compiled RE pattern for configured topic.
        """
        return self.re_for_topic(self.topic)


class ApiConfig(pydantic.BaseModel):
    """
    Temperature API configuration
    """

    address: str = "http://localhost/"
    token: str = ""


class CacheConfig(pydantic.BaseModel):
    """
    Cache configuration where records will be stored when API is not available.
    """

    enabled: bool = True
    path: str = ":memory:"
    purge_interval: float = 60


class Config(pydantic.BaseSettings):
    """
    App configuration.
    """

    mqtt: MqttConfig = MqttConfig()
    api: ApiConfig = ApiConfig()
    cache: CacheConfig = CacheConfig()
    logging: RootLogging = RootLogging()

    class Config:
        """ ENV configuration """

        env_nested_delimiter = '_'


class Cache:
    """
    Reading cache configuration
    """

    _DB_VERSION = 1

    def __init__(self, config: Config, loop: asyncio.AbstractEventLoop):
        """
        :param config: App configuration
        :param loop: Event loop to use
        """
        self.db: asyncio.Future[aiosqlite.Connection] = loop.create_future()
        self.config = config
        self.loop = loop
        self.db_lock = asyncio.Lock()
        self._cache_task: Optional[asyncio.Task] = None

        if self.config.cache.enabled:
            loop.create_task(self._open_database())
            self._cache_task = loop.create_task(self._cache_purge_task())

    def stop(self) -> None:
        """
        Request the cache to stop.
        """
        if self._cache_task:
            self._cache_task.cancel()

    async def close(self) -> None:
        """
        Do cleanup of the cache.
        """
        if self._cache_task and not self._cache_task.cancelled():
            self._cache_task.cancel()
            try:
                await self._cache_task

            except asyncio.CancelledError:
                pass

            except Exception as exc:  # pylint: disable=broad-except
                logging.exception(exc)

        if self.config.cache.enabled:
            await (await self.db).close()

    async def _migrate_db(self, db: aiosqlite.Connection) -> None:
        """
        Migrate the database to current version.
        :param db: Database connection to migrate
        """
        cursor = await db.cursor()
        await cursor.execute("PRAGMA user_version")
        row = await cursor.fetchone()
        version = row[0] if row else 0

        # For the future, at least one version of backward compatibility of database should be OK.
        if version > self._DB_VERSION + 1:
            raise RuntimeError("Cache database is too new for this version to work with. Consider upgrading the"
                               "application.")

        if version < 1:
            # Migrate to DB v1
            await cursor.execute("CREATE TABLE `reading_cache` (`date` TEXT, `device` TEXT, `value` TEXT)")

        await cursor.execute(f"PRAGMA main.user_version = {self._DB_VERSION}")

    async def _open_database(self):
        """
        Open the database and migrate it to neweset version.
        """
        try:
            logger = logging.root.getChild("cache")

            def log_statement(query: str) -> None:
                logger.debug("Executing %s...", query)

            db = await aiosqlite.connect(self.config.cache.path, loop=self.loop)
            await db.set_trace_callback(log_statement)
            db.row_factory = sqlite3.Row

            await self._migrate_db(db)

            self.db.set_result(db)

            logger.info("Using cache %s", self.config.cache.path)

        except Exception as exc:  # pylint: disable=broad-except
            self.db.set_exception(exc)

    async def _cache_purge_task(self) -> None:
        """
        Task that periodically tries to send cached readings to API.
        """
        try:
            async with aiohttp.ClientSession(self.config.api.address, loop=self.loop) as session:
                logger = logging.root.getChild("cache")
                while True:
                    try:
                        readings = await self.load()
                        if readings:
                            await publish_or_store_readings(readings, self.config, session, self)

                    except asyncio.CancelledError:  # pylint: disable=try-except-raise
                        raise

                    except Exception as exc:  # pylint: disable=broad-except
                        logger.exception(exc)

                    finally:
                        logger.debug("Waiting %f seconds before trying again...", self.config.cache.purge_interval)
                        await asyncio.sleep(self.config.cache.purge_interval)
        except asyncio.CancelledError:
            pass

    async def store(self, readings: Iterable[Reading]) -> bool:
        """
        Store readings to cache, if enabled.
        :param readings: Readings to store
        :return: True if readings were stored, False if cache is disabled.
        """
        logger = logging.root.getChild("cache")

        if not self.config.cache.enabled:
            logger.warning("Discarding readings %r because cache is disabled.", readings)
            return False

        db = await self.db
        async with self.db_lock:
            try:
                await db.execute("BEGIN TRANSACTION")

                for reading in readings:
                    await db.execute(
                        "INSERT INTO `reading_cache` "
                        "(`date`, `device`, `value`) "
                        "VALUES (?, ?, ?)",
                        [reading.date, reading.device, reading.value]
                    )
                    logger.debug("Stored %s in cache.", reading.json())

                await db.commit()
                return True
            finally:
                if db.in_transaction:
                    await db.rollback()

    async def load(self) -> List[Reading]:
        """
        Load readings from cache. Retrieved readings are purged from the database and not retrieved by subsequent
        calls to load() anymore.
        :return: List of cached readings.
        """
        if not self.config.cache.enabled:
            return []

        db = await self.db
        async with self.db_lock:
            out: List[Reading] = []

            try:
                logger = logging.root.getChild("cache")

                await db.execute("BEGIN TRANSACTION")

                async with db.execute("SELECT `date`, `device`, `value` FROM `reading_cache`") as cursor:
                    async for row in cursor:
                        out.append(Reading(device=row["device"], value=row["value"], date=row["date"]))

                await db.execute("DELETE FROM `reading_cache`")
                await db.commit()

                if out:
                    logger.debug("Retrieved %s from cache", out)

                return out
            finally:
                if db.in_transaction:
                    await db.rollback()


async def publish_readings(readings: Iterable[Reading], api: ApiConfig, session: aiohttp.ClientSession) -> bool:
    """
    Publish readings to API.
    :param readings: Readings to publish
    :param api: Configuration of API
    :param session: Session to API server
    :return: True if readings were published successfully. False otherwise.
    """
    headers = {
        "X-Grid": grid.get_or_create(f"collect-mqtt-{socket.gethostname()}-{str(uuid4())}"),
        "Content-Type": "application/json",
    }

    logger = logging.root.getChild("api")

    if api.token:
        headers["X-Auth-Token"] = api.token

    try:
        data = Readings(__root__=list(readings))
        endpoint = urljoin(api.address, "api")

        logger.debug("Publishing %s at %s", data.json(), endpoint)

        response = await session.post(
            "/api",
            headers=headers,
            data=data.json()
        )

        # Receive whole response before continuing.
        txt = await response.text()

        if response.status == 200:
            for reading in readings:
                logger.info("Successfully published: device=%s value=%fC date=%s", reading.device, reading.value, reading.date)
            return True

        logger.error("Unable to post readings to API: %s", txt)
        return False

    except ClientConnectorError as exc:
        logger.error(exc)
        return False

    except Exception as exc:  # pylint: disable=broad-except
        logger.exception(exc)
        return False


async def store_readings(readings: Sequence[Reading], cache: Cache) -> bool:
    """
    Store readings to cache.
    :param readings: Readings to store.
    :param cache: Cache where to store the readings.
    :return: True if readings were stored successfully.
    """
    try:
        res = await cache.store(readings)
        if res:
            if len(readings) > 1:
                logging.info("Readings stored in cache.")
            elif len(readings) == 1:
                logging.info("Reading stored in cache.")

        return res

    except Exception as exc:  # pylint: disable=broad-except
        logging.root.getChild("cache").exception(exc)
        return False


async def publish_or_store_readings(readings: Sequence[Reading], config: Config, session: aiohttp.ClientSession, cache: Cache) -> bool:
    """
    Publish readings or store them in cache if publish failed.
    :param readings: Readings to publish
    :param config: App configuration
    :param session: HTTP session to use
    :param cache: Cache to use
    :return: True if readings was processed (either pushed or cached), False if it was discarded.
    """
    try:
        if not await publish_readings(readings, config.api, session):
            return await store_readings(readings, cache)

        return True

    # pylint: disable=broad-except
    except Exception as exc:
        logging.exception(exc)
        return await store_readings(readings, cache)


# pylint: disable=too-many-arguments
async def process_single_message(message: MQTTMessage, config: Config, session: aiohttp.ClientSession, cache: Cache,
                                 logger: logging.Logger, loop: asyncio.AbstractEventLoop) -> None:
    """
    Process single message received from MQTT.
    :param message:
    :param config:
    :param session:
    :param cache:
    :param logger:
    :param loop:
    :return:
    """
    logger.debug("Received %s: %s", message.topic, message.payload.decode())

    match = config.mqtt.topic_re.match(message.topic)
    if match:
        groups = match.groupdict()

        payload = message.payload.decode()
        sensor = groups["sensor"]
        unit = groups.get("unit", "C")

        logging.debug("Received reading for %s: %s %s", sensor, payload, unit)

        loop.create_task(publish_or_store_readings(
            [Reading(
                device=sensor,
                value=payload
            )],
            config,
            session,
            cache
        ))
    else:
        logger.warning("Topic not parsed: %s", message.topic)


async def mqtt_listener_single(config: Config, cache: Cache, loop: asyncio.AbstractEventLoop) -> None:
    """
    Single MQTT listener instance without retrying failed MQTT connection.
    :param config: App configuration
    :param cache: Cache to use
    :param loop: Loop to use
    :return:
    """
    logger = logging.root.getChild("mqtt-listener")
    mqtt = config.mqtt

    try:
        async with aiohttp.ClientSession(config.api.address, loop=loop) as session:

            logger.info("Connecting to MQTT broker at %s:%d...", mqtt.host, mqtt.port)
            async with Client(
                    mqtt.host,
                    mqtt.port,
                    username=mqtt.user,
                    password=mqtt.password,
                    client_id=mqtt.client_id,
                    will=Will(
                        mqtt.lwt_topic,
                        mqtt.lwt_offline.encode(),
                        retain=mqtt.lwt_retain
                    ) if mqtt.lwt_topic and mqtt.lwt_offline else None,
            ) as mqtt_conn:
                try:
                    if mqtt.lwt_topic and mqtt.lwt_online:
                        await mqtt_conn.publish(mqtt.lwt_topic, payload=mqtt.lwt_online.encode(), retain=mqtt.lwt_retain)

                    topic = mqtt.topic.format(sensor="+", device="+", unit="+")
                    await mqtt_conn.subscribe(topic)

                    async with mqtt_conn.unfiltered_messages() as messages:
                        logger.info("Connected to MQTT broker at %s:%d, waiting for messages at %s", mqtt.host,
                                    mqtt.port, topic)

                        async for message in messages:
                            await process_single_message(message, config, session, cache, logger, loop)

                except MqttError as exc:
                    logger.error("MQTT: %s", exc)

                finally:
                    if mqtt.lwt_topic and mqtt.lwt_offline:
                        await mqtt_conn.publish(mqtt.lwt_topic, payload=mqtt.lwt_offline.encode(), retain=mqtt.lwt_retain)

    except MqttError as exc:
        logger.error("While connecting to %s:%d: %s", mqtt.host, mqtt.port, exc)


async def mqtt_listener(config: Config, cache: Cache, loop: asyncio.AbstractEventLoop) -> None:
    """
    MQTT listener task. As soon as configured topic is received, it is parsed and published to API or cached for
    further processing, depending on API availability and configuration.
    :param config: App configuration
    :param cache: Cache to use
    :param loop: Loop to use
    """
    try:
        while True:
            await mqtt_listener_single(config, cache, loop)
            await asyncio.sleep(1)

    except asyncio.CancelledError:
        pass

    except Exception as exc:  # pylint: disable=broad-except
        logging.exception(exc)


def fill_api_config(args: Namespace, api: ApiConfig) -> None:
    """
    Fill API configuration from ArgParse namespace.
    :param args: Namespace
    :param api: API configuration
    """
    if args.api_address:
        api.address = args.api_address

    if args.api_token:
        api.token = args.api_token


def fill_mqtt_lwt(args: Namespace, mqtt: MqttConfig) -> None:
    """
    Fill in LWT topic configuration.
    :param args: Namespace
    :param mqtt: MQTT configuration
    """
    if args.mqtt_lwt:
        mqtt.lwt_topic = args.mqtt_lwt

    if args.mqtt_lwt_online:
        mqtt.lwt_online = args.mqtt_lwt_online

    if args.mqtt_lwt_offline:
        mqtt.lwt_offline = args.mqtt_lwt_offline

    if args.mqtt_lwt_retain_yes:
        mqtt.lwt_retain = True

    if args.mqtt_lwt_retain_no:
        mqtt.lwt_retain = False


def fill_mqtt_config(args: Namespace, mqtt: MqttConfig) -> None:
    """
    Fill MQTT configuration from ArgParse namespace.
    :param args: Namespace
    :param mqtt: MQTT configuration
    """
    if args.mqtt_host:
        mqtt.host = args.mqtt_host

    if args.mqtt_port:
        mqtt.port = args.mqtt_port

    if args.mqtt_user:
        mqtt.user = args.mqtt_user

    if args.mqtt_password:
        mqtt.password = args.mqtt_password

    if args.mqtt_client_id:
        mqtt.client_id = args.mqtt_client_id

    if args.mqtt_topic:
        mqtt.topic = args.mqtt_topic

    fill_mqtt_lwt(args, mqtt)


def fill_cache_config(args: Namespace, cache: CacheConfig) -> None:
    """
    Fill cache configuration from ArgParse namespace.
    :param args: Namespace
    :param cache: Cache configuration
    """
    if args.cache_path:
        cache.path = args.cache_path
        cache.enabled = True

    if args.cache_enabled_no:
        cache.enabled = False


def parse_command_line_args(config: Config) -> None:
    """
    Parse command line arguments and fill in configuration objects based on command line arguments.
    :param config: Configuration to fill
    """
    parser = ArgumentParser(add_help=False)
    parser.add_argument("--help", help="Show this help and quit.", dest="show_help", action="store_true")

    parser.add_argument("--api", "-a", help="API endpoint address", dest="api_address")
    parser.add_argument("--token", "-k", help="API key", dest="api_token")
    parser.add_argument("--host", "-h", help="MQTT host", dest="mqtt_host")
    parser.add_argument("--port", "-p", help="MQTT port", type=int, dest="mqtt_port")
    parser.add_argument("--user", "-u", help="MQTT user", dest="mqtt_user")
    parser.add_argument("--password", "-P", help="MQTT password", dest="mqtt_password")
    parser.add_argument("--client-id", "-c", help="MQTT client ID", dest="mqtt_client_id")
    parser.add_argument("--topic", "-t", help="MQTT topic to subscribe to. {sensor} will be replaced by sensor name. "
                                              "{device} will be replaced by device name. {unit} will be replaced by "
                                              "unit. Can contain MQTT wildcards.", dest="mqtt_topic")
    parser.add_argument("--lwt", "-w", help="Last will topic.", dest="mqtt_lwt")
    parser.add_argument("--lwt-online", help="Last will online payload", dest="mqtt_lwt_online")
    parser.add_argument("--lwt-offline", help="Last will offline payload", dest="mqtt_lwt_offline")
    parser.add_argument("--lwt-retain", help="Retain LWT topic", dest="mqtt_lwt_retain_yes", action="store_true")
    parser.add_argument("--no-lwt-retain", help="Do not retain LWT topic.", dest="mqtt_lwt_retain_no",
                        action="store_true")

    parser.add_argument("--cache", "-d", help="Path to filename where to store cached records.", dest="cache_path")
    parser.add_argument("--no-cache", help="Do not cache entries if API is unreachable.", dest="cache_enabled_no",
                        action="store_true")

    args = parser.parse_args()

    if args.show_help:
        parser.print_help()
        sys.exit(1)

    fill_api_config(args, config.api)
    fill_mqtt_config(args, config.mqtt)
    fill_cache_config(args, config.cache)


async def run(loop) -> None:
    """
    Asynchronous app entrypoint.
    :param loop: Loop to run on
    """
    config = Config()
    config.logging.loggers["aiosqlite"] = Logging(level=LogLevel.INFO)

    parse_command_line_args(config)
    config.logging.apply()

    cache = Cache(config, loop)

    mqtt_task = loop.create_task(mqtt_listener(config, cache, loop))
    quit_requested: bool = False

    def request_quit():
        nonlocal quit_requested

        if not quit_requested:
            logging.info("Terminating gracefully. Ctrl+C to force quit.")
            mqtt_task.cancel()
            cache.stop()
        else:
            sys.exit(1)

    loop.add_signal_handler(signal.SIGINT, request_quit)
    loop.add_signal_handler(signal.SIGTERM, request_quit)

    await mqtt_task

    logging.info("Performing cleanup...")

    loop.remove_signal_handler(signal.SIGINT)
    loop.remove_signal_handler(signal.SIGTERM)

    await cache.close()

    logging.info("All done, bye.")


def main() -> None:  # noqa: D401
    """
    Synchronous app entrypoint.
    """
    loop = asyncio.new_event_loop()
    loop.run_until_complete(run(loop))
    sys.exit(0)


if __name__ == "__main__":
    main()
