"""
Logging configuration models.
"""

from __future__ import annotations

import logging
import logging.handlers
import sys
import warnings
from abc import ABC
from enum import Enum
from typing import Dict, Optional, cast

from pydantic import BaseModel, Field

from grid import GRID


class LogLevel(Enum):
    """
    Logging level
    """

    INHERIT: None = None
    DEBUG = logging.DEBUG
    INFO = logging.INFO
    WARNING = logging.WARNING
    ERROR = logging.ERROR
    CRITICAL = logging.CRITICAL

    @classmethod
    def __get_validators__(cls):
        """
        Yield all validators for this class.
        """
        yield cls.validate

    @classmethod
    def validate(cls, value):
        """
        Allow population of LogLevel by name.
        """
        try:
            if isinstance(value, LogLevel):
                return value

            return cls.__members__[value] if value in cls.__members__ else LogLevel(value)
        except KeyError as exc:
            raise ValueError(f"Invalid logging level: {value!r}") from exc


class ColorLoggerMixin(logging.Handler, ABC):
    """
    Mixin for logging.Handler that outputs colored messages.
    """

    def format(self, record):
        color_start = {
            logging.DEBUG: "\033[2m",
            logging.WARNING: "\033[33m",
            logging.ERROR: "\033[31m",
            logging.CRITICAL: "\033[1;31m",
            logging.INFO: "\033[39m"
        }.get(record.levelno, "")
        color_end = "\033[0m"

        msg = super().format(record)
        msg = msg.replace(color_end, f"{color_end}{color_start}")
        msg = "\n".join([f"{color_start}{line}{color_end}" for line in msg.splitlines()])

        return msg


class GridLoggerMixin(logging.Handler, ABC):
    """
    Mixin for logging.Handler that adds GRID to the log record.
    """

    def format(self, record):
        record.GRID = GRID.get() or "-"
        return super().format(record)


class StreamHandler(ColorLoggerMixin, GridLoggerMixin, logging.StreamHandler):
    """
    StreamHandler with GridLoggerMixin and ColorLoggerMixin.
    """


class QueueHandler(ColorLoggerMixin, GridLoggerMixin, logging.handlers.QueueHandler):
    """
    QueueHandler with GridLoggerMixin and ColorLoggerMixin.
    """


class Logging(BaseModel):
    """
    Logging configuration
    """

    level: LogLevel = LogLevel.INHERIT
    output: str = Field("", description="Where to log. If empty, inherits from parent logger. If STDERR/STDOUT, "
                                        "writes to standard error / standard output. Otherwise treats field as file.")
    format: str = Field("", description="Logging message format. If empty, inherits from parent logger.")

    loggers: Dict[str, Logging] = {}
    parent: Optional[Logging] = None

    @property
    def resolved_format(self) -> str:
        """
        Return applicable format with respect to parent loggers and inheritance.
        """
        return self.format if self.format else self.parent.resolved_format if self.parent else ""

    @property
    def debug(self) -> bool:
        """
        Is log level debug?
        """
        return self.level == LogLevel.DEBUG \
            if self.level != LogLevel.INHERIT \
            else self.parent.debug \
            if self.parent \
            else False

    def apply(self, logger: logging.Logger) -> None:
        """
        Apply configuration to logger.
        :param logger: Logger to apply to.
        """
        if self.level != LogLevel.INHERIT:
            lvl: int = cast(int, self.level.value)
            logger.setLevel(lvl)

        formatter = logging.Formatter(self.resolved_format)

        if self.output or (logger == logging.root and not logger.hasHandlers()):
            logger.handlers = []

            # pylint: disable=consider-using-with
            handler = StreamHandler(
                sys.stderr
                if self.output.upper() == "STDERR"
                else sys.stdout if self.output.upper() == "STDOUT"
                else open(self.output, "a", encoding="utf-8")
            )
            handler.setFormatter(formatter)

            logger.addHandler(handler)
        elif logger.hasHandlers():
            for existing_handler in logger.handlers:
                existing_handler.setFormatter(formatter)

        for logger_name, config in self.loggers.items():
            config.parent = self
            sublogger = logger.getChild(logger_name)
            config.apply(sublogger)


class RootLogging(Logging):
    """
    Configuration of the root logger
    """

    level: LogLevel = LogLevel.DEBUG
    output: str = Field("STDERR", description="STDERR / STDOUT logs to standard error / standard output, other values means file name to write to.")
    format: str = Field("%(asctime)s %(levelname)s: [%(GRID)s] %(message)s {%(filename)s:%(lineno)s|%(name)s}", description="Logging format.")

    def apply(self, logger: logging.Logger = logging.root) -> None:
        """
        Apply configuration to logger.
        :param logger: Logger to apply to (root logger)
        """
        if logger == logging.root:
            logging.captureWarnings(True)
            warnings.filterwarnings("always", category=RuntimeWarning)
            warnings.formatwarning = lambda msg, *args, **kwargs: str(msg)

        return super().apply(logger)
