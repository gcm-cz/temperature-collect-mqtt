# MQTT Collector for Temperature API

MQTT Collector connects to MQTT broker, listens for messages on specified topic pattern and for each received message,
pushes payload to the Temperature API.

It expects the topic to contain a sensor identifier, and value is plain float number as temperature reading.

## Requirements

### Docker

Best deployed as Docker daemon, so just `docker build . -t temperature-collect-mqtt` and you are ready to go.

### Stand-alone Python

When running as stand-alone script, **Python 3.10+** is required. Install (preferably to venv) packages from
`requirements.txt` (`pip install -r requirements.txt`).

## Deployment

### Docker

Run the built docker image and specify configuration as environment variables.

`docker run -i -e MQTT_HOST=my.mqtt.broker -e API_ADDRESS=http://temperature.example.com temperature-collect-mqtt`

### Stand-alone Python

Just execute `collect_mqtt/collect_mqtt.py`, optionally specifying command line arguments, or environment variables.
No systemd unit file is provided, as Docker is the preferred deployment way.


## Configuration

Configuration can be passed as command line arguments. See `collect_mqtt.ph --help`. Also, environment variables can be
used to provide configuration. List of env variables is following:

- `MQTT_HOST` - Default: `localhost`
- `MQTT_PORT` - Default: `1883`
- `MQTT_USER` - Default: *None*
- `MQTT_PASSWORD` - Default: *None*
- `MQTT_CLIENT_ID` - Default: *randomly generted*
- `MQTT_LWT_TOPIC` - Will topic. Default: *None*
- `MQTT_LWT_ONLINE` - Will payload for running application. Default: `Online`
- `MQTT_LWT_OFFLINE` - Will payload when application is not running. Default: `Offline`
- `MQTT_LWT_RETAIN` - When set to `1`, Will topic will be retained. Defalt: `1`
- `MQTT_TOPIC` - Topic pattern for readings. Default: `sensorio/{device}/channel/{sensor}/{unit}`. Placeholders are 
   used to extract values for reading from the topic.


- `API_ADDRESS` - Temperature API endpoint. Default: `http://localhost/`
- `API_TOKEN` - API access token. Default: *None*


- `CACHE_ENABLED` - Set to `1` to enable caching of failed readings. Set to `0` to disable caching. Default: `1` 
- `CACHE_PATH` - Filesystem path where to store the cache. If set to `:memory:`, only in-memory cache will be used, 
  which is not persisted between application restarts. Default: `:memory:`
- `CACHE_PURGE_INTERVAL` - How often, in seconds, the app will make attempt to send readings
  from cache. In seconds. Default: `60`


- LOGGING_LEVEL - Logging level. Can be one of `DEBUG`, `INFO`, `WARNING`, `ERROR`, `CRITICAL`. Default: `DEBUG`
- LOGGING_OUTPUT - Log output. File path or `STDERR` for standard error, `STDOUT` for standard output. Default: `STDERR`
- LOGGING_FORMAT - Logging format. See https://docs.python.org/3/library/logging.html#logrecord-attributes for more info. Default: *Reasonable default is used.*
