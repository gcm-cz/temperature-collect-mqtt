FROM python:3.10
ADD requirements.txt /
RUN pip install -r requirements.txt

WORKDIR /collect_mqtt/
COPY collect_mqtt/ /collect_mqtt/

CMD ["/collect_mqtt/collect_mqtt.py"]